/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persondb;

import java.io.Serializable;

/**
 *
 * @author informatics
 */
public class Person implements Serializable {
    private String username;
    private String name;
    private String surname;
    private String password;
    private String tel;
    private int age;
    private int height;
    private int weight;

    public Person(String username, String name, String surname, String password, String tel, int age, int height, int weight) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.tel = tel;
        this.age = age;
        this.height = height;
        this.weight = weight;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Person{" + "username=" + username + ", name=" + name + ", surname=" + surname + ", password=" + password + ", tel=" + tel + ", age=" + age + ", height=" + height + ", weight=" + weight + '}';
    }
    
    
    
}

